FROM maven:3.6.3-adoptopenjdk-11 AS builder
COPY . .
#RUN ./mvnw package -Dquarkus.package.type=uber-jar
RUN ./mvnw package

#FROM oracle/graalvm-ce:20.0.0
FROM registry.access.redhat.com/ubi8/ubi-minimal:8.1
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
COPY --from=builder target/*-runner.jar /app.jar
# run the application
CMD [ "java", "-jar", "/app.jar" ]




